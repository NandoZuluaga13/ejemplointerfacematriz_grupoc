/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Interface.*;
import Negocio.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madar
 */
public class PruebaMatrizAleatorio {
 
    public static void main(String[] args) {
        
        try {
            System.out.println("Por favor digite datos de la matriz:");
            Scanner lector = new Scanner(System.in);
            System.out.println("Por favor digite cantidad de filas:");
            int filas=lector.nextInt();
            System.out.println("Por favor digite cantidad de columnas:");
            int cols=lector.nextInt();
            System.out.println("Por favor digite limiInicial de elementos aleatorios:");
            int ini=lector.nextInt();
            System.out.println("Por favor digite limifinal de elementos aleatorios:");
            int fin=lector.nextInt();
            
            MatrizEntero_Aleatoria myMatriz=new MatrizEntero_Aleatoria(filas, cols);
            myMatriz.crearMatriz(ini, fin);
            System.out.println("Mi matriz:"+"\n"+myMatriz.toString());
            /*
            // Forma1:
            System.out.println("Total de la forma1:"+myMatriz.getSumaTotal());
            
            //Forma 2:POLIMORFISMO
            IMatriz i_matriz=myMatriz;
            System.out.println("Total de la forma2:"+i_matriz.getSumaTotal());
            
            //Forma 3:CASTING DE LA INTERFACE
            
            int total=((IMatriz)myMatriz).getSumaTotal();
            System.out.println("Total de la forma3:"+total);
            
            
            Par_Numero par=new Par_Numero(4,5);
            //Forma2:
            IMatriz i2=par;
            System.out.println("Total de la forma2:"+i2.getSumaTotal());
            
            
            VectorNumero vector=new VectorNumero(5,ini,fin);
            //Forma2:
            IMatriz iMatriz=vector;
            System.out.println("Total de la forma2:"+iMatriz.getSumaTotal());
            */
            
            //INTERFAZ 2
            
            //Forma1
            System.out.println("Numero que mas se repite en la matriz: "+myMatriz.getMas_Se_Repite());
            
            //Forma 2:POLIMORFISMO
            Par_Numero par2= new Par_Numero(2,2);
            IMatriz2 iMatriz2=par2;
            System.out.println("Numero que mas se repite : "+iMatriz2.getMas_Se_Repite());
            
            
            //Forma 3:CASTING DE LA INTERFACE
            VectorNumero v1= new VectorNumero(4, ini, fin);
            
            int numero=((IMatriz2)v1).getMas_Se_Repite();
            System.out.println("Numero que mas se repite en el vector: "+numero);
            
            
            
            
        } catch (Exception ex) {
            System.err.println("Error:"+ex.getMessage());
        }
    }
}
